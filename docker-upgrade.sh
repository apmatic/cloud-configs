#!/bin/bash -e
# Select which Docker version to use on CoreOS with torcx.

# Specify the available Docker version to enable.
version=17.03

# Create modifiable torcx paths if they don't exist already.
mkdir -p /etc/torcx/profiles /var/lib/torcx/store

# Download the torcx manifest file for the currently running OS version.
. /usr/share/coreos/release
manifest="https://tectonic-torcx.release.core-os.net/manifests/$COREOS_RELEASE_BOARD/$COREOS_RELEASE_VERSION/torcx_manifest.json"
wget -P /tmp "$manifest".asc "$manifest"

# Verify its signature with the CoreOS application signing key.
export GNUPGHOME=$(mktemp -d)
trap 'rm -fr "$GNUPGHOME"' EXIT
gpg2 --keyserver pool.sks-keyservers.net --recv-keys 18AD5014C99EF7E3BA5F6CE950BDD3E0FC8A365E
gpg2 --verify /tmp/torcx_manifest.json.asc /tmp/torcx_manifest.json

# Download the selected Docker image at its URL in the manifest.
wget -P /var/lib/torcx/store $(jq -r ".value.packages[] | select(.name == \"docker\") | .versions[] | select(.version == \"${version}\") | .locations[] | select(.url).url" < /tmp/torcx_manifest.json)
test "x$(jq -r ".value.packages[] | select(.name == \"docker\") | .versions[] | select(.version == \"${version}\") | .hash" < /tmp/torcx_manifest.json)" = "xsha512-$(sha512sum "/var/lib/torcx/store/docker:${version}.torcx.tgz" | sed 's/ .*//')"

# Write a new profile named "docker" that selects the desired version on boot.
sed "s/com.coreos.cl/$version/g" /usr/share/torcx/profiles/vendor.json > /etc/torcx/profiles/docker.json
echo docker > /etc/torcx/next-profile

# Reboot to start using the new version.
reboot
