**Um Änderungen durchzuführen sollte IMMER [Ansible][ansible-link] verwendet werden, Dateien sollten nicht direkt auf den Hosts angepasst werden**

# Bezeichnungsschema

_Siehe `ìnventory.yaml` für Details!_


# Notwendige Schritte für einen neuen Host

Damit der neue Host via Ansible verwaltet und aktualisiert werden kann, ist ein einmaliger _Bootstrap_ notwendig.

`ansible-playbook -i inventory.yaml -l app bootstrap.yaml`


# Konfiguration (cloud-config) auf den Hosts aktualisieren

Aktualisierung aller Konfigurationen auf allen Systemen:
`ansible-playbook -i inventory.yaml -l app copy-cloud-config.yaml`

_Die neuen Konfigurationen werden erst nach einem Neustart der Systeme aktiv!_

### Direktes anpassen einer cloud-config (normalerweise nicht notwendig!):
1. Via SSH auf den gewünschten CoreOS-Server verbinden. Z.b `ssh core01`.
2. Die momentan Aktive Konfig. öffnen mit `sudo vi /var/lib/coreos-install/user_data`.
3. Den Inhalt der Datei löschen mit folgem Vi-Befehl: `:%d`.
4. Nun `i` drücken um den Insert-Mode in Vi zu aktivieren.
5. Kompletten Inhalte der entsprechenden `core0X.yaml` Datei kopieren und im Terminal einfügen.
**Darauf achten, dass die RICHTIGE Datei verwendet wird!**
6. `ESC` drücken um den Insert-Mode zu verlassen und mit dem Vi-Befehl `:wq` die Datei speichern. 
7. Server neustarten und Änderung(en) verifizieren.


# Iptables aktualisieren ohne System-Neustart

Beispiel um eine neue IP den uneingeschränkten Zugriff zu gewähren:
`ansible -i inventory.yaml app -m shell -a "sudo iptables -I INPUT -s 88.99.13.139 -j ACCEPT"`

_Vorgängig muss jeweils die geänderte Konfigrationen aktualisiert werden (siehe weiter oben)!_ 


[ansible-link]: http://docs.ansible.com/ansible/latest/intro.html